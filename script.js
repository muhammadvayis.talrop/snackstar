function showSidebar() {
    const sidebar = document.querySelector(".sidebar");
    const menu = document.querySelector(".menu");
    const close = document.querySelector(".close");
    close.style.display = "block";
    menu.style.display = "none";
    sidebar.style.display = "flex";
}

function hideSidebar() {
    const sidebar = document.querySelector(".sidebar");
    const close = document.querySelector(".close");
    const menu = document.querySelector(".menu");
    menu.style.display = "block";
    close.style.display = "none";
    sidebar.style.display = "none";
}


// // Function to fetch and display products
// fetch('https://fakestoreapi.com/products').then((data)=>{
//     // console.log(data);
//     return data.json();
// }).then((completedata)=>{
//     // console.log(completedata);
//     let data1="";
//     completedata.map((values)=>{
//         data1+=`<div class="card">
//         <h1 class="title">${values.title}</h1>
//         <div class="photo">
//             <img src=${values.image} alt="image" />
//         </div>
//         <p class="text">${values.description}</p>
//         <p class="category">${values.category}</p>
//         <p class="price">${values.price}</p>
//     </div>`;
//     });
//     document.getElementById("cards").innerHTML=data1;


// }).catch((error)=>{
//     console.log(error);
// });



document.addEventListener('DOMContentLoaded', function() {
    const viewOptions = document.querySelectorAll('input[name="view"]');
    const cardsContainer = document.getElementById('cards');

    // Function to fetch and display products
    function fetchAndDisplayProducts(view) {
        fetch('https://fakestoreapi.com/products')
            .then((response) => response.json())
            .then((data) => {
                let sortedData = data;
                if (view === '2') {
                    sortedData = data.sort((a, b) => a.category.localeCompare(b.category));
                }

                let cardsHTML = '';
                sortedData.forEach((product) => {
                    cardsHTML += `<div class="card">
                        <h1 class="title">${product.title}</h1>
                        <div class="photo">
                            <img src="${product.image}" alt="image" />
                        </div>
                        <p class="text">${product.description}</p>
                        <p class="category">${product.category}</p>
                        <p class="price">$${product.price}</p>
                    </div>`;
                });
                cardsContainer.innerHTML = cardsHTML;
            })
            .catch((error) => console.log(error));
    }

    // Event listener for view options
    viewOptions.forEach((option) => {
        option.addEventListener('change', function() {
            document.documentElement.style.setProperty('--cards-per-row', this.value);
            fetchAndDisplayProducts(this.value);
        });
    });

    // Initial fetch
    fetchAndDisplayProducts('2');
});


